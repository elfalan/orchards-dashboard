export const ADD_SENSOR = `ADD_SENSOR`;
export const REMOVE_SENSOR = `REMOVE_SENSOR`;
export const UPDATE_SENSOR = `UPDATE_SENSOR`;
export const SET_ACTIVE_SENSOR = `SET_ACTIVE_SENSOR`;

let currentNewSensorId = 0;

export const addSensor = (lat, lng) => {
  currentNewSensorId ++;
  return {
    type: ADD_SENSOR,
    sensor: { lat, lng, id: currentNewSensorId }
  }
}

export const removeSensor = (id) => {
  return (dispatch, getState) => {
    const newSensors = getState().sensors.newSensors.filter(sensor => {
      return sensor.id !== id;
    });
    dispatch({
      type: REMOVE_SENSOR,
      sensors: newSensors
    });
  }
}

export const updateSensor = (id, lat, lng) => {
  return (dispatch, getState) => {
    const newSensors = getState().sensors.newSensors.filter(sensor => {
      return sensor.id !== id;
    });
    newSensors.push({ id, lat, lng });
    dispatch({
      type: UPDATE_SENSOR,
      sensors: newSensors
    });
  }
}

export const setActiveSensor = (id) => {
  return (dispatch, getState) => {
    const activeSensor = getState().sensors.sensors.filter(sensor => {
      return sensor.id === id;
    })[0];
    dispatch({
      type: SET_ACTIVE_SENSOR,
      sensor: activeSensor
    });
  }
}

export const removeActiveSensor = () => {
  return {
    type: SET_ACTIVE_SENSOR,
    sensor: null
  }
}