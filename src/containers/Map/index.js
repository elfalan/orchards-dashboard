import React, { Component } from 'react';
import { connect } from 'react-redux';

import MapComponent from '../../components/Map';
import SensorMarker from '../SensorMarker';
import NewSensorMarker from '../NewSensorMarker';

import { addSensor } from '../../actions/mapActions';

class Map extends Component {
  constructor(props) {
    super(props);

    this.handleMapClick = this.handleMapClick.bind(this);
  }

  handleMapClick(event) {
    this.props.addSensor(event.latLng.lat(), event.latLng.lng());
  }

  render() {
    const {
      sensors,
      newSensors,
      activeSensor
    } = this.props;

    return (
      <MapComponent handleMapClick={this.handleMapClick} activeSensor={activeSensor}>
        {sensors.map(sensor => 
          <SensorMarker key={sensor.id} {...sensor} />
        )}
        {newSensors.map((sensor, index) =>
          <NewSensorMarker key={sensor.id} {...sensor} /> 
        )}
      </MapComponent>
    )
  }
}

const mapStateToProps = state => {
  return {
    sensors: state.sensors.sensors,
    newSensors: state.sensors.newSensors,
    activeSensor: state.sensors.activeSensor
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addSensor: (lat, lng) => {
      dispatch(addSensor(lat, lng));
    }
  }
}

const ConnectedMap = connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);

export default ConnectedMap;