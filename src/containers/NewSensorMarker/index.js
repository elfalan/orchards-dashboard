import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Marker } from 'react-google-maps';

import { removeSensor, updateSensor } from '../../actions/mapActions';

class NewSensorMarker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id
    }

    this.handleDragEnd = this.handleDragEnd.bind(this);
  }

  handleDragEnd(event) {
    const id = this.state.id;
    const lat = event.latLng.lat();
    const lng = event.latLng.lng();
    this.props.updateSensor(id, lat, lng);
  }
  
  render() {
    const {
      lat,
      lng,
      id
    } = this.props;

    return (
      <Marker 
        position={{ lat, lng }}
        icon='http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        draggable={true}
        onDragEnd={this.handleDragEnd}
        onDblClick={() => this.props.removeSensor(id)}>
      </Marker>
    );
  }
}

const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {
    removeSensor: (id) => {
      dispatch(removeSensor(id));
    },
    updateSensor: (id, lat, lng) => {
      dispatch(updateSensor(id, lat, lng));
    }
  }
}

const ConnectedNewSensorMarker = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewSensorMarker);

export default ConnectedNewSensorMarker;