import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Grid, InputLabel } from "material-ui";

import {
    ProfileCard,
    RegularCard,
    Button,
    CustomInput,
    ItemGrid
  } from "../../components";

import avatar from "../../assets/img/faces/marc.jpg";

  class SensorFieldInput extends Component {
      constructor(props){
          super(props);
      }
  

  render(){

    return (
        <div>
          <Grid container>
            <ItemGrid xs={12} sm={12} md={12}>
              <RegularCard
                cardTitle="Add New Sensor"
                cardSubtitle="Fill in the fields for the sensor and click add"
                content={
                  <div>
                    <Grid container>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Sensor ID"
                          id="id"
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Type"
                          id="type"
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Latitude"
                          id="lat"
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Longitude"
                          id="lng"
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Farm"
                          id="farm"
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                      <ItemGrid xs={12} sm={12} md={4}>
                        <CustomInput
                          labelText="Label"
                          id="label"
                          formControlProps={{
                            fullWidth: true
                          }}
                        />
                      </ItemGrid>
                    </Grid>
                    {/* <Grid container>
                      <ItemGrid xs={12} sm={12} md={12}>
                        <InputLabel style={{ color: "#AAAAAA" }}>
                          About me
                        </InputLabel>
                        <CustomInput
                          labelText="Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo."
                          id="about-me"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            multiline: true,
                            rows: 5
                          }}
                        />
                      </ItemGrid>
                    </Grid> */}
                  </div>
                }
                footer={<Button color="primary">Add Sensor</Button>}
              />
            </ItemGrid>
            {/* <ItemGrid xs={12} sm={12} md={4}>
              <ProfileCard
                avatar={avatar}
                subtitle="CEO / CO-FOUNDER"
                title="Alec Thompson"
                description="Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is..."
                footer={
                  <Button color="primary" round>
                    Follow
                  </Button>
                }
              />
            </ItemGrid> */}
          </Grid>
        </div>
      );
    }
}

export default SensorFieldInput;
