import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from "material-ui";
import { RegularCard, Table, ItemGrid } from "../../components";


class SensorList extends Component {
  render() {
    const {
      sensors
    } = this.props;

    return(
      <Grid container>
      <ItemGrid xs={12} sm={12} md={12}>
        <RegularCard
          cardTitle="Sensor List"
          cardSubtitle="Sensors for Farm A"
          content={
            <Table
              tableHeaderColor="primary"
              tableHead={["ID", "Type", "Latitude", "Longitude", "Farm", "Label"]}
              tableData={
                
                sensors.map(sensor => {
                   return (
                    [sensor.id,sensor.type, sensor.lat, sensor.lng, sensor.farm,sensor.label]
                        );
                      }) 
                      
              }
            />
          }
        />
      </ItemGrid>
      </Grid>
    );

    //return (
      // <div id='sensors_table_container'>
      //   <table id='sensors_table'>
      //     <thead>
      //       <tr>
      //         <th>ID</th>
      //         <th>Type</th>
      //         <th>Latitude</th>
      //         <th>Longitude</th>
      //         <th>Farm</th>
      //         <th>Label</th>
      //       </tr>
      //     </thead>
      //     <tbody>
      //       {sensors.map(sensor => {
      //         return (
      //           <tr key={sensor.id}>
      //             <td>{sensor.id}</td>
      //             <td>{sensor.type}</td>
      //             <td>{sensor.lat}</td>
      //             <td>{sensor.lng}</td>
      //             <td>{sensor.farm}</td>
      //             <td>{sensor.label}</td>
      //           </tr>
      //         );
      //       })}
      //     </tbody>
      //   </table>
      // </div>
   // );
  }
}

const mapStateToProps = state => {
  return {
    sensors: state.sensors.sensors
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

const ConnectedSensorList = connect(
  mapStateToProps,
  mapDispatchToProps
)(SensorList);

export default ConnectedSensorList;