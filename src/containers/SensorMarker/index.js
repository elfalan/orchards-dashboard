import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Marker, InfoWindow } from 'react-google-maps';

import { setActiveSensor, removeActiveSensor } from '../../actions/mapActions';

class SensorMarker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id,
      isOpen: false
    }

    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.handleMarkerClose = this.handleMarkerClose.bind(this);
  }

  handleMarkerClick() {
    this.setState({ isOpen: true });
    this.props.setActiveSensor(this.state.id);
  }

  handleMarkerClose() {
    this.setState({ isOpen: false });
    this.props.setActiveSensor()
  }

  render() {
    const {
      id,
      lat,
      lng
    } = this.props;

    return (
      <Marker position={{ lat, lng }} onClick={this.handleMarkerClick}>
        {this.state.isOpen && 
        <InfoWindow onCloseClick={this.handleMarkerClose}>
          <div>{id}</div>
        </InfoWindow>}
      </Marker>
    );
  }
}

const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {
    setActiveSensor: (id) => {
      dispatch(setActiveSensor(id));
    },
    removeActiveSensor: () => {
      dispatch(removeActiveSensor());
    }
  }
}

const ConnectedSensorMarker = connect(
  mapStateToProps,
  mapDispatchToProps
)(SensorMarker);

export default ConnectedSensorMarker;