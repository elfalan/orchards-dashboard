import React, { 
    Component,
    } from 'react';
import Map from '../../containers/Map';
import SensorList from '../../containers/SensorList';

import '../SensorList/styles.css';
const styles = {
    title: {
        fontSize: 40,
        fontWeight: '300',
        color: 'red',
    },
};

class MapContainer extends Component {
  render() {
    return (
      <div className="MapContainer">
        <Map />
        <SensorList />
      </div>
    );
  }
}

export default MapContainer;