import React from 'react';
import ReactDOM from 'react-dom';
//import App from './containers/app';
import MapContainer from './containers/mapContainer';
import registerServiceWorker from './registerServiceWorker';
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
//import { composeWithDevtools } from 'redux-devtools-extension';
import reducers from './reducers';

import "./assets/css/material-dashboard-react.css?v=1.2.0";

import indexRoutes from "./routes/index.js";

const boundCompose = compose.bind(null, applyMiddleware(thunk));
const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ ? boundCompose(window.__REDUX_DEVTOOLS_EXTENSION__()) : boundCompose()
);

const hist = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
  {/* <App /> */}
  {/* <MapContainer /> */}
  <Router history={hist}>
    <Switch>
      {indexRoutes.map((prop, key) => {
        return <Route path={prop.path} component={prop.component} key={key} />;
      })}
    </Switch>
  </Router>
    
  </Provider>, 
  document.getElementById('root'));
registerServiceWorker();
