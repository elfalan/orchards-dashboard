import { combineReducers } from 'redux';
import sensorsReducer from './sensorsReducer';

export default combineReducers({
  sensors: sensorsReducer
});