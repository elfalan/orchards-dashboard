import sensors from '../assets/sensors'
import { ADD_SENSOR, REMOVE_SENSOR, UPDATE_SENSOR, SET_ACTIVE_SENSOR } from '../actions/mapActions';

const initialState = {
  sensors,
  newSensors: [],
  activeSensor: null
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_SENSOR:
      return { ...state, newSensors: state.newSensors.concat(action.sensor) };
    case REMOVE_SENSOR:
    case UPDATE_SENSOR:
      return { ...state, newSensors: action.sensors };
    case SET_ACTIVE_SENSOR:
      return { ...state, activeSensor: action.sensor };
    default:
      return state;
  }
}