import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";
import React, { 
  Component,
  } from 'react';
import Map from '../../containers/Map';
import SensorList from '../../containers/SensorList';
import SensorFieldInput from '../../containers/SensorFieldInput';

import '../../containers/SensorList/styles.css';
const styles = {
  title: {
      fontSize: 40,
      fontWeight: '300',
      color: 'red',
  },
};

function Maps({ ...props }) {
  return (
    <div className="App">
    <SensorFieldInput />
    <Map />
    <SensorList />
  </div>
  );
}

export default Maps;
